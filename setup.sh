#!/bin/zsh
echo Installing dependencies
# TODO: Install neovim beta release
brew install coreutils curl git nodejs neovim the_silver_searcher fzf


echo Installing oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo Installing plugins for oh-my-zsh
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting


echo Installing asdf
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.0


echo Setup symlinks
DOTFILES=~/.dotfiles
ln -s $DOTFILES/tmux.conf ~/.tmux.conf
ln -s $DOTFILES/zshrc ~/.zshrc
ln -s $DOTFILES/editorconfig ~/.editorconfig
ln -s $DOTFILES/vim ~/.vim


echo Setup neovim
CONFIG=~/.config/nvim
mkdir $CONFIG
touch $CONFIG/init.vim

cat > $CONFIG/init.vim<< EOF
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vim/vimrc
EOF

echo Setup coc.nvim
ln -s ~/.dotfiles/coc-settings.json ~/.config/nvim/coc-settings.json


echo DONE!
echo Now start nvim and run :PlugInstall to install plugins.
