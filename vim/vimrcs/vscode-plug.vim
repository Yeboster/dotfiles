call plug#begin('~/.vim/plugged')
  Plug 'tpope/vim-repeat'
  Plug 'justinmk/vim-sneak'
  Plug 'tpope/vim-surround'
  Plug 'asvetliakov/vim-easymotion'
call plug#end()
