call plug#begin('~/.vim/plugged')
  Plug 'neoclide/coc.nvim', {'branch': 'release'}

  " File navigation
  Plug 'mileszs/ack.vim'
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'
  " Plug 'ctrlpvim/ctrlp.vim'
  Plug 'tpope/vim-commentary'
  Plug 'tpope/vim-eunuch'

  " Git stuff
  Plug 'tpope/vim-fugitive'
  Plug 'idanarye/vim-merginal'
  Plug 'rbong/vim-flog'

  Plug 'nikvdp/neomux'
  Plug 'tpope/vim-repeat'
  Plug 'tpope/vim-surround'
  Plug 'jiangmiao/auto-pairs'
  Plug 'justinmk/vim-sneak'
  Plug 'terryma/vim-multiple-cursors'
  Plug 'editorconfig/editorconfig-vim'
  Plug 'junegunn/goyo.vim'
  Plug 'honza/vim-snippets'
  Plug 'nathanaelkane/vim-indent-guides'
  Plug 'luochen1990/rainbow'
  Plug 'tpope/vim-rails'
  Plug 'sheerun/vim-polyglot'
  Plug 'andrewstuart/vim-kubernetes'
  Plug 'glepnir/dashboard-nvim'

  " Neovim in browser
  Plug 'glacambre/firenvim'

  " Themes
  Plug 'sainnhe/gruvbox-material'
  Plug 'morhetz/gruvbox'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'sheerun/vim-polyglot'
call plug#end()
