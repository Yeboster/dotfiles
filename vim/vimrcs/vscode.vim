"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Compatible Version With VSCode:
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Show special characters
set list

set autoindent

let mapleader = ","

" Fast saving
nmap <leader>w :w<cr>

" Manage tabs
" Create new tab
nmap <leader>tn :tabnew<cr>
nmap <leader>tc :tabclose<cr>

" Comment line
xmap gc  <Plug>VSCodeCommentary
nmap gc  <Plug>VSCodeCommentary
omap gc  <Plug>VSCodeCommentary
nmap gcc <Plug>VSCodeCommentaryLine

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase
