# Welcome to Yeboster dotfiles!

Since all those file are centralized, I highly recommend to create a symlink.

# Setup

## Automatic
Just run this command to get everything done.
```
chmod +x setup.sh && ./setup.sh
```

# Coc integrations

To use correctly Coc, link the configuration file with the following command:

```
cd ~/.config/nvim/
ln -s ~/.dotfiles/vim/coc-settings.json .
```

### Coc Explorer

I wanted to try another explorer so instead of using nerdtree, I'm using coc-explorer now. To install this extension run:

```
:CocInstall coc-explorer
```

To open explorer type `ge` on normal mode.

### Coc git

Integrate git with coc using the extension:
```
:CocInstall coc-git
```

### Coc Todo List

Since I want to have a list of all actions to do and done (used a lot gitlab issues, but I'm lazy), I'm using a coc extension.

```
:CocInstall coc-todolist
```

It support uploading using github gists (not bad).

Useful commands:
- :CocCommand todolist.create: create a new todo
- :CocCommand todolist.upload: upload todolist to gist
- :CocCommand todolist.download: download todolist from gist
- :CocCommand todolist.export: export todolist as a json/yaml file
- :CocCommand todolist.clearNotice: clear all notifications

### Coc Diagnostics

This extension integrates liters and formatters and it's provided by NodeJS. To install run:

```
yarn global add diagnostic-languageserver
```

On vim install this coc extension:

```
:CocInstall coc-diagnostic
```

The coc config is already updated and can be added a lot of linters or formatters.

Several linters are used, like:

#### Write good linter (spell checker)

Using write good to improve my english writing.

Write good is an NodeJS extension, so be sure it's installed by running: `yarn global add write-good`.


### Yaml

Install the yaml extension with `:CocInstall coc-yaml`.

### Elm

To integrate correcly elm install the language server with this command:

```
yarn global add elm elm-test
```

Further information [here](https://github.com/elm-tooling/elm-language-server)

### Dockerfile

Since I'm a hardcore user of docker, I've added an extension for dockerfiles.

Install the extension:

```
yarn global add dockerfile-language-server-nodejs
```

Next, install the Coc extension inside nvim with `:CocInstall coc-docker`

Finally, add the relative Coc configuration:
```
{
  "languageserver": {
    "dockerfile": {
          "command": "docker-langserver",
          "filetypes": ["dockerfile"],
          "args": ["--stdio"]
    }
  }
}
```

### Elixir

Elixir is a fun and smart language when interacting with web and networks (first impression). To improve my experience with that, I'm using a coc-extension.

First of all, install the language and the analyzer:

↓ Fedora ↓
```
sudo dnf install elixir erlang-dialyzer
```

The language server is already included inside
Next, install the elixir coc-extension:
```
:CocInstall coc-elixir
```
If the language server has some problems, you can install it manually by executing:
```
cd ~/ && curl -LO https://github.com/JakeBecker/elixir-ls/releases/download/v0.2.25/elixir-ls.zip &&
unzip elixir-ls.zip -d ~/.elixir-ls &&
chmod +x ~/.elixir-ls/language_server.sh &&
rm -f ~/elixir-ls.zip
```

The coc config is already included inside the json file.
