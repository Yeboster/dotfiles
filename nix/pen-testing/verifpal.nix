{ pkgs ? import <nixpkgs> {} }:
let
  stdenv = pkgs.stdenv;
  fetchurl = pkgs.fetchurl;
in
stdenv.mkDerivation {
  name = "verifpal-0.13.3";
  src = fetchurl {
    url = https://source.symbolic.software/verifpal/verifpal/uploads/bf1e87322000ee67558a0010f1def073/verifpal_0.13.3_linux_amd64.zip;
    sha256 = "58f3be873d9c392a7bdff8f8c83135d48a7152f35072bdf8ffdde666d7247e0d";
  };

  buildInputs = with pkgs; [ unzip ];

  unpackPhase = ''
    mkdir -p $out/bin
    unzip -p $src '*/verifpal' > $out/bin/verifpal
    chmod +x $out/bin/verifpal
  '';

  dontInstall = true;

}
