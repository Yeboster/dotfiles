{ pkgs ? import <nixpkgs> {} }:
let
  IDEs = with pkgs; [
    neovim
  ];

  languages = with pkgs; [
    ruby
    python3
    nodejs
  ];

  libraries = {
    hugo = import ./dev/hugo.nix {};
  };

  # Pull out values of the set
  libraryBins = builtins.attrValues libraries;

in pkgs.mkShell {
  name = "devbench-box";
  buildInputs = IDEs ++ languages ++ libraryBins;
}
