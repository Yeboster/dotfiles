{ pkgs ? import <nixpkgs> {} }:
let
    shellPackages = with pkgs; [
        file
        gzip
        bzip2
        xxd
    ];

    networkPackages = with pkgs; [
        telnet
        openssl
        netcat # nc
        nmap
        gobuster # Dir scanner
    ];

    languagePackages = with pkgs; [
      python3
      php
    ];

    binaryPackages = with pkgs; [
      ltrace
      radare2
      gdb
      rr
    ];

    cryptoPackages = with pkgs; { 
      # Verify security of cryptographic protocols
      verifpal = import ./pen-testing/verifpal.nix {};
    };

    forensicsPackages = with pkgs; [
      exiftool # Extract meta information from files
    ];

    exploitPackages = with pkgs; [
      sqlmap # SQL injections
    ];

    devPackages = with pkgs; [
        tmux
        screen
        git
        neovim
    ];

    dictLibraries = builtins.attrValues cryptoPackages;

    packages = shellPackages ++
               networkPackages ++
               languagePackages ++
               binaryPackages ++
               exploitPackages ++
               dictLibraries ++
               forensicsPackages ++
               devPackages;
in
pkgs.mkShell {
    name = "hacking-box";
    buildInputs = packages;
}
